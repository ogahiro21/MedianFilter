# Median Filter
入力画像をメディアンフィルタ処理する  

## About Median filter
局所領域の濃度値を順番に並べてその中央値を取る方法。  
画素数が偶数のときは、中央の2値の平均を取る。  
インパルス性ノイズの除去に有効なフィルター。

## Description
* 処理ウィンドウサイズは3×3とする。
* 画像領域内の値だけで行う。
* 出力値は0~255となるように処理する。

## Median filter
- 入力画像  
![in10](https://gitlab.com/ogahiro21/MedianFilter/raw/image/image/in10.jpeg)  
- 出力画像  
![out10](https://gitlab.com/ogahiro21/MedianFilter/raw/image/image/out10.jpeg)

## Usage
**コンパイル**
```
gcc -o cpbmp median_filter.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in10.bmp ans10.bmp
```
