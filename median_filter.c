#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "var.h"
#include "bmpfile.h"

#define NUM 5
#define SIZE 256

// 注目画素の更新値を求める関数
int  calc_around_pixel(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                       int x, int y, int color, int around_pixel);

// ソート
void sort(double array[], int around_pixel);

int main(int argc, char *argv[])
{
  imgdata idata;
  int x, y;
  int around_pixel = 8;

  if (argc < 3) printf("使用法：cpbmp コピー元.bmp コピー先.bmp\n");
  else {
    if (readBMPfile(argv[1], &idata) > 0)
    printf("指定コピー元ファイル%sが見つかりません\n",argv[1]);
    else {
      for (y = 0; y < idata.height; y++) {
        for (x = 0; x <idata.width; x++) {
          // 周辺画素の数を計算
          if(x%(SIZE-1) == 0 && y%(SIZE-1) == 0) around_pixel = 4;
          else if(x%(SIZE-1) == 0 || y%(SIZE-1) == 0) around_pixel = 6;
          else around_pixel = 9;

          // 出力画像に格納
          idata.results[RED][y][x]
          = calc_around_pixel(idata.source, x, y, RED, around_pixel);
          idata.results[GREEN][y][x]
          = calc_around_pixel(idata.source, x, y, GREEN, around_pixel);
          idata.results[BLUE][y][x]
          = calc_around_pixel(idata.source, x, y, BLUE, around_pixel);
        }
      }

      if (writeBMPfile(argv[2], &idata) > 0)
      printf("コピー先ファイル%sに保存できませんでした\n",argv[2]);
    }
  }
}

int  calc_around_pixel(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                       int x, int y, int color, int around_pixel)
{
  int i, j, count = 0;
  double ans;
  double array[8];

  while(count < around_pixel){
    for (i = -1; i < 2; i++) {
      for (j = -1; j < 2; j++) {
        // 配列外でないか判定
        if (y+i < SIZE && y+i >=0 && x+j < SIZE && x+j >=0 ) {
          // 配列に格納
          array[count] = source[color][y+i][x+j];
          count++;
        }
      }
    }
  }

  // ソート
  sort(array,around_pixel);

  // 配列の中心を算出
  switch (around_pixel) {
    case 4:
    ans = (array[1] + array[2]) /2 ;
    break;

    case 6:
    ans = (array[2] + array[3]) / 2;
    break;

    case 9:
    ans = array[4];
    break;

    default:
    printf("error\n" );
    exit(1);
    break;
  }

  return ans;
}

void swap(double array[], int target){
  double tmp;

  tmp = array[target];
  array[target] = array[target -1];
  array[target-1] = tmp;
}

void sort(double array[], int around_pixel){
  int i, j;

  for ( i = 1; i < around_pixel; i++) {
    for (j = i; j >= 0; j--) {
      if(array[j-1] > array[ j ]) {
        swap(array, j);
      }
      else break;
    }
  }
}
